Project Title: 
Software System to Automate Examination Question Paper Generation

1. Abstract:
The project is to develop a software system to automate the process of generating examination question paper by establishing a question bank for a given course (Data Communication) offered by The Computer and IT Department to 2nd year students in the 4th semester for 3 credits. It has 3 examinations (Test One Exam T1, Test Two Exam T2, End Semester Exam).
